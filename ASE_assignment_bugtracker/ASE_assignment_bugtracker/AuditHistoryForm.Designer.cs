﻿namespace ASE_assignment_bugtracker
{
    partial class AuditHistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstBugAudits = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnOpenAuditDetail = new System.Windows.Forms.Button();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lstBugAudits
            // 
            this.lstBugAudits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lstBugAudits.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBugAudits.FullRowSelect = true;
            this.lstBugAudits.Location = new System.Drawing.Point(0, 1);
            this.lstBugAudits.MultiSelect = false;
            this.lstBugAudits.Name = "lstBugAudits";
            this.lstBugAudits.Size = new System.Drawing.Size(929, 331);
            this.lstBugAudits.TabIndex = 0;
            this.lstBugAudits.UseCompatibleStateImageBehavior = false;
            this.lstBugAudits.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Bug ID";
            this.columnHeader1.Width = 72;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Audited on";
            this.columnHeader2.Width = 123;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Audited by";
            this.columnHeader3.Width = 111;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Bug resolved";
            this.columnHeader4.Width = 130;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Comment";
            this.columnHeader5.Width = 488;
            // 
            // btnOpenAuditDetail
            // 
            this.btnOpenAuditDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenAuditDetail.Location = new System.Drawing.Point(695, 1);
            this.btnOpenAuditDetail.Name = "btnOpenAuditDetail";
            this.btnOpenAuditDetail.Size = new System.Drawing.Size(234, 29);
            this.btnOpenAuditDetail.TabIndex = 1;
            this.btnOpenAuditDetail.Text = "View bug audit detail";
            this.btnOpenAuditDetail.UseVisualStyleBackColor = true;
            this.btnOpenAuditDetail.Click += new System.EventHandler(this.btnOpenAuditDetail_Click);
            // 
            // columnHeader6
            // 
            this.columnHeader6.DisplayIndex = 5;
            this.columnHeader6.Text = "ID";
            this.columnHeader6.Width = 0;
            // 
            // AuditHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 334);
            this.Controls.Add(this.btnOpenAuditDetail);
            this.Controls.Add(this.lstBugAudits);
            this.Name = "AuditHistoryForm";
            this.Text = "Bug tracker";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstBugAudits;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnOpenAuditDetail;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}