﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using ASE_assignment_bugtracker.Classes;

namespace ASE_assignment_bugtracker
{
    /* This form is split up into two parts, logically divided between the TabPages of a TabControl. 
     * The first tab allows the user to report a new bug, offering various controls to submit the required information.
     * The second tab lists all the bugs from the database, offering the option to view the selected bug in greater
     * detail, starting an audit for the selected bug, and viewing the selected bug's audit history. */
    public partial class MainForm : Form
    {
        private string titlePlaceholder = "Give a clear title for this bug";
        private string descPlaceholder = "Please describe as detailed as possible how the bug can be reproduced. Cause, symptoms, how it affects the program, ...";
        private DBConnector db;
        public MainForm()
        {
            InitializeComponent();
            txtTitle.Text = titlePlaceholder;
            txtDescription.Text = descPlaceholder;
            /* these event handlers are used to fake a "placeholder"-text, because this is not natively implemented in
             * .NET textboxes. When a textbox receives focus, the palceholder-text will be removed. When the textbox
             * loses again, the placeholder will be set back, if the user has not yet filled in another value (that is
             * not empty). */
            txtTitle.GotFocus += txtTitle_GotFocus;
            txtTitle.LostFocus += txtTitle_LostFocus;
            txtDescription.GotFocus += txtDescription_GotFocus;
            txtDescription.LostFocus += txtDescription_LostFocus;
            tabControl1.SelectedIndexChanged += tabControl1_SelectedIndexChanged;

            db = new DBConnector();
        }

        

        

        private void populateListView()
        {
            /* when switching to the second tab, fill up the ListView with all the bugs from the database */
            foreach (ListViewItem lvi in lstBugs.Items)
            {
                // ListView.Clear() also removes the predefined columns which is not desired, this loop pretty much fakes the .Clear() functionality by only removing the actual ListItems
                lstBugs.Items.Remove(lvi);
            }
            foreach (Bug b in db.getAllBugs())
            {
                ListViewItem bugItem = new ListViewItem(b.ID.ToString());
                bugItem.SubItems.Add(b.Status);
                bugItem.SubItems.Add(b.BugDetails.Title);
                bugItem.SubItems.Add(b.BugDetails.Description);
                bugItem.SubItems.Add(b.BugDetails.ReportedBy);
                bugItem.SubItems.Add(b.BugDetails.ReportedDate.ToString());
                lstBugs.Items.Add(bugItem);
            }
        }

        void txtTitle_GotFocus(object sender, EventArgs e)
        {
            if(txtTitle.Text.Contains(titlePlaceholder))
                txtTitle.Text = "";
        }
        void txtTitle_LostFocus(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtTitle.Text))
                txtTitle.Text = titlePlaceholder;
        }
        void txtDescription_GotFocus(object sender, EventArgs e)
        {
            if (txtDescription.Text.Contains(descPlaceholder))
                txtDescription.Text = "";
        }
        void txtDescription_LostFocus(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtDescription.Text))
                txtDescription.Text = descPlaceholder;
        }
        void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* when the second TabPage is selected, fill up its ListView */
            if ((sender as TabControl).SelectedIndex == 1)
            {
                populateListView();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            /* constructs a new Bug-object and BugDetails-object based on the information supplied in the form's 
             * controls, after checking if the required fields are filled in. Then, the name of the reported is asked 
             * through an InputBox, after which it is submitted and inserted in the database. */
            if (String.IsNullOrWhiteSpace(txtTitle.Text) && String.IsNullOrWhiteSpace(txtDescription.Text))
            {
                MessageBox.Show("Please supply a valid title and description for the bug.");
                return;
            }
            string reporterName = Microsoft.VisualBasic.Interaction.InputBox("Please supply your name below:", "Bug reporter name", "UserName");
            if (String.IsNullOrWhiteSpace(reporterName))
            {
                MessageBox.Show("Please supply a valid name.");
                return;
            }
            int lineStart, lineEnd;
            BugDetails newBugDetails = new BugDetails(txtTitle.Text.Trim(), txtDescription.Text.Trim(), txtSourceFile.Text,
                txtMethodName.Text, (!int.TryParse(txtLineNumberStart.Text, out lineStart)) ? -1 : lineStart, (!int.TryParse(txtLineNumberEnd.Text, out lineEnd)) ? -1 : lineEnd, txtProjectName.Text,
                reporterName, DateTime.Now);
            Bug newBug = new Bug("Open", newBugDetails);
            db.insertNewBug(newBug);
            MessageBox.Show("The bug has been reported.");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            /* button to clear all text currently filled in in the form's controls/set placeholders back */
            txtTitle.Text = titlePlaceholder;
            txtDescription.Text = descPlaceholder;
            foreach (Control x in groupBox1.Controls)
            {
                if (x is TextBox)
                {
                    ((TextBox)x).Text = String.Empty;
                }
            }
        }


        private void btnStartBugAudit_Click(object sender, EventArgs e)
        {
            openDetailsForm(true); /* opens the DetailForm, notifying it that it will be used to display a BugAudit,
                                    * causing the DetailsForm to show the relevant controls */
        }

        private void btnViewBugDetails_Click(object sender, EventArgs e)
        {
            openDetailsForm(); /* opens the DetailForm to view the selected Bug's details */
        }

        private void openDetailsForm(bool audit = false)
        {
            if (lstBugs.SelectedItems.Count > 0)
            {
                var item = lstBugs.SelectedItems[0];
                Bug selectedBug = db.getBug(int.Parse(item.Text));
                if (selectedBug == null)
                {
                    // the DBConnector.getBug(id)-method returns null if no Bug has been found with the given id
                    MessageBox.Show("An error occurred while trying to retrieve this bug's information.");
                    return;
                }
                DetailsForm df = new DetailsForm(selectedBug, audit);
                df.Show();
            }
        }

        private void btnViewBugHistory_Click(object sender, EventArgs e)
        {
            /* shows the audit history of the selected Bug in the AuditHistoryForm */
            if (lstBugs.SelectedItems.Count > 0)
            {
                var item = lstBugs.SelectedItems[0];
                Bug selectedBug = db.getBug(int.Parse(item.Text));
                if (selectedBug == null)
                {
                    MessageBox.Show("An error occurred while trying to retrieve this bug's information.");
                    return;
                }
                if (db.getBugAudits(selectedBug).Count <= 0)
                {
                    // if no audits have been performed for this bug, there's no use in showing the form, a message is displayed instead
                    MessageBox.Show("This bug has not been audited yet.");
                    return;
                }
                AuditHistoryForm ahf = new AuditHistoryForm(selectedBug);
                ahf.Show();
            }
        }

    }
}
