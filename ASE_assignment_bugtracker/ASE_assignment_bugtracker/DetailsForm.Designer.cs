﻿namespace ASE_assignment_bugtracker
{
    partial class DetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkResolved = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAuditComment = new System.Windows.Forms.TextBox();
            this.grpBugDetails = new System.Windows.Forms.GroupBox();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtReportDate = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtReportedBy = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.txtLineEnd = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLineStart = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMethodName = new System.Windows.Forms.TextBox();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.grpAudit = new System.Windows.Forms.GroupBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtAuditDate = new System.Windows.Forms.TextBox();
            this.grpBugDetails.SuspendLayout();
            this.grpAudit.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkResolved
            // 
            this.chkResolved.AutoSize = true;
            this.chkResolved.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkResolved.Location = new System.Drawing.Point(17, 30);
            this.chkResolved.Name = "chkResolved";
            this.chkResolved.Size = new System.Drawing.Size(191, 24);
            this.chkResolved.TabIndex = 0;
            this.chkResolved.Text = "Mark bug as resolved";
            this.chkResolved.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Comment:";
            // 
            // txtAuditComment
            // 
            this.txtAuditComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuditComment.Location = new System.Drawing.Point(17, 106);
            this.txtAuditComment.Multiline = true;
            this.txtAuditComment.Name = "txtAuditComment";
            this.txtAuditComment.Size = new System.Drawing.Size(403, 286);
            this.txtAuditComment.TabIndex = 2;
            // 
            // grpBugDetails
            // 
            this.grpBugDetails.Controls.Add(this.txtStatus);
            this.grpBugDetails.Controls.Add(this.label6);
            this.grpBugDetails.Controls.Add(this.txtID);
            this.grpBugDetails.Controls.Add(this.label7);
            this.grpBugDetails.Controls.Add(this.txtReportDate);
            this.grpBugDetails.Controls.Add(this.label4);
            this.grpBugDetails.Controls.Add(this.txtReportedBy);
            this.grpBugDetails.Controls.Add(this.label5);
            this.grpBugDetails.Controls.Add(this.txtProjectName);
            this.grpBugDetails.Controls.Add(this.txtLineEnd);
            this.grpBugDetails.Controls.Add(this.label3);
            this.grpBugDetails.Controls.Add(this.txtLineStart);
            this.grpBugDetails.Controls.Add(this.label2);
            this.grpBugDetails.Controls.Add(this.txtMethodName);
            this.grpBugDetails.Controls.Add(this.txtSourceFile);
            this.grpBugDetails.Controls.Add(this.txtDescription);
            this.grpBugDetails.Controls.Add(this.txtTitle);
            this.grpBugDetails.Location = new System.Drawing.Point(13, 13);
            this.grpBugDetails.Name = "grpBugDetails";
            this.grpBugDetails.Size = new System.Drawing.Size(466, 452);
            this.grpBugDetails.TabIndex = 3;
            this.grpBugDetails.TabStop = false;
            this.grpBugDetails.Text = "Bug details";
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.ForeColor = System.Drawing.Color.Black;
            this.txtStatus.Location = new System.Drawing.Point(243, 17);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(204, 27);
            this.txtStatus.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(180, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 20);
            this.label6.TabIndex = 24;
            this.label6.Text = "Status";
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.ForeColor = System.Drawing.Color.Black;
            this.txtID.Location = new System.Drawing.Point(63, 18);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(111, 27);
            this.txtID.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 20);
            this.label7.TabIndex = 22;
            this.label7.Text = "ID";
            // 
            // txtReportDate
            // 
            this.txtReportDate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtReportDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReportDate.ForeColor = System.Drawing.Color.Black;
            this.txtReportDate.Location = new System.Drawing.Point(303, 405);
            this.txtReportDate.Name = "txtReportDate";
            this.txtReportDate.ReadOnly = true;
            this.txtReportDate.Size = new System.Drawing.Size(144, 27);
            this.txtReportDate.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(270, 409);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 20);
            this.label4.TabIndex = 20;
            this.label4.Text = "on";
            // 
            // txtReportedBy
            // 
            this.txtReportedBy.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtReportedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReportedBy.ForeColor = System.Drawing.Color.Black;
            this.txtReportedBy.Location = new System.Drawing.Point(122, 405);
            this.txtReportedBy.Name = "txtReportedBy";
            this.txtReportedBy.ReadOnly = true;
            this.txtReportedBy.Size = new System.Drawing.Size(142, 27);
            this.txtReportedBy.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 405);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 20);
            this.label5.TabIndex = 18;
            this.label5.Text = "Reported by";
            // 
            // txtProjectName
            // 
            this.txtProjectName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtProjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProjectName.ForeColor = System.Drawing.Color.Black;
            this.txtProjectName.Location = new System.Drawing.Point(16, 369);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.ReadOnly = true;
            this.txtProjectName.Size = new System.Drawing.Size(273, 27);
            this.txtProjectName.TabIndex = 17;
            // 
            // txtLineEnd
            // 
            this.txtLineEnd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtLineEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineEnd.ForeColor = System.Drawing.Color.Black;
            this.txtLineEnd.Location = new System.Drawing.Point(242, 325);
            this.txtLineEnd.Name = "txtLineEnd";
            this.txtLineEnd.ReadOnly = true;
            this.txtLineEnd.Size = new System.Drawing.Size(111, 27);
            this.txtLineEnd.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(195, 328);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "to";
            // 
            // txtLineStart
            // 
            this.txtLineStart.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtLineStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineStart.ForeColor = System.Drawing.Color.Black;
            this.txtLineStart.Location = new System.Drawing.Point(63, 325);
            this.txtLineStart.Name = "txtLineStart";
            this.txtLineStart.ReadOnly = true;
            this.txtLineStart.Size = new System.Drawing.Size(111, 27);
            this.txtLineStart.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 328);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Line";
            // 
            // txtMethodName
            // 
            this.txtMethodName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtMethodName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMethodName.ForeColor = System.Drawing.Color.Black;
            this.txtMethodName.Location = new System.Drawing.Point(241, 283);
            this.txtMethodName.Name = "txtMethodName";
            this.txtMethodName.ReadOnly = true;
            this.txtMethodName.Size = new System.Drawing.Size(206, 27);
            this.txtMethodName.TabIndex = 12;
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtSourceFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSourceFile.ForeColor = System.Drawing.Color.Black;
            this.txtSourceFile.Location = new System.Drawing.Point(16, 283);
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.ReadOnly = true;
            this.txtSourceFile.Size = new System.Drawing.Size(207, 27);
            this.txtSourceFile.TabIndex = 11;
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.ForeColor = System.Drawing.Color.Black;
            this.txtDescription.Location = new System.Drawing.Point(16, 84);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(431, 178);
            this.txtDescription.TabIndex = 10;
            // 
            // txtTitle
            // 
            this.txtTitle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.Black;
            this.txtTitle.Location = new System.Drawing.Point(16, 51);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.ReadOnly = true;
            this.txtTitle.Size = new System.Drawing.Size(431, 27);
            this.txtTitle.TabIndex = 0;
            // 
            // grpAudit
            // 
            this.grpAudit.Controls.Add(this.txtAuditDate);
            this.grpAudit.Controls.Add(this.btnSubmit);
            this.grpAudit.Controls.Add(this.chkResolved);
            this.grpAudit.Controls.Add(this.label1);
            this.grpAudit.Controls.Add(this.txtAuditComment);
            this.grpAudit.Location = new System.Drawing.Point(517, 13);
            this.grpAudit.Name = "grpAudit";
            this.grpAudit.Size = new System.Drawing.Size(437, 452);
            this.grpAudit.TabIndex = 4;
            this.grpAudit.TabStop = false;
            this.grpAudit.Text = "Audit ";
            this.grpAudit.Visible = false;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(314, 398);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(106, 43);
            this.btnSubmit.TabIndex = 3;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtAuditDate
            // 
            this.txtAuditDate.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtAuditDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuditDate.ForeColor = System.Drawing.Color.Black;
            this.txtAuditDate.Location = new System.Drawing.Point(227, 28);
            this.txtAuditDate.Name = "txtAuditDate";
            this.txtAuditDate.ReadOnly = true;
            this.txtAuditDate.Size = new System.Drawing.Size(204, 27);
            this.txtAuditDate.TabIndex = 26;
            this.txtAuditDate.Visible = false;
            // 
            // DetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(966, 485);
            this.Controls.Add(this.grpAudit);
            this.Controls.Add(this.grpBugDetails);
            this.Name = "DetailsForm";
            this.Text = "Bug tracker";
            this.grpBugDetails.ResumeLayout(false);
            this.grpBugDetails.PerformLayout();
            this.grpAudit.ResumeLayout(false);
            this.grpAudit.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkResolved;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAuditComment;
        private System.Windows.Forms.GroupBox grpBugDetails;
        private System.Windows.Forms.GroupBox grpAudit;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtMethodName;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.TextBox txtLineEnd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLineStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.TextBox txtReportDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtReportedBy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAuditDate;
    }
}