﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ASE_assignment_bugtracker.Classes;

namespace ASE_assignment_bugtracker
{
    /* This form serves to display all of the BugAudits performed for a chosen Bug, listing them up in the ListView.
     * A single audit can be selected and can be viewed in detail.*/
    public partial class AuditHistoryForm : Form
    {
        private DBConnector db;
        private Bug bug;
        public AuditHistoryForm(Bug selectedBug)
        {
            InitializeComponent();
            bug = selectedBug;
            db = new DBConnector();
            populateListView();
        }

        private void populateListView()
        {
            // fills the ListView up with all the BugAudits performed for the selected Bug from the MainForm
            foreach (BugAudit ba in db.getBugAudits(bug))
            {
                ListViewItem auditItem = new ListViewItem(ba.ID.ToString());
                auditItem.SubItems.Add(bug.ID.ToString());
                auditItem.SubItems.Add(ba.AuditDate.ToString());
                auditItem.SubItems.Add(ba.AuditedBy);
                auditItem.SubItems.Add((ba.IsResolved) ? "Yes" : "No");
                auditItem.SubItems.Add(ba.Comment);
                lstBugAudits.Items.Add(auditItem);
            }
        }

        private void btnOpenAuditDetail_Click(object sender, EventArgs e)
        {
            /* if an item (BugAudit) has been selected from the ListView, the BugAudit-object is retrieved based on its
             * id, and the audit details and related bug details are then shown in the DetailsForm.*/
            if (lstBugAudits.SelectedItems.Count > 0)
            {
                var item = lstBugAudits.SelectedItems[0];
                BugAudit selectedAudit = db.getBugAudit(int.Parse(item.Text));
                if (selectedAudit == null)
                {
                    // the DBConnector.getBugAudit(id)-method returns null if no BugAudit has been found with the given id
                    MessageBox.Show("An error occurred while trying to retrieve this audit's information.");
                    return;
                }
                DetailsForm df = new DetailsForm(selectedAudit.Bug, false, selectedAudit);
                df.Show();
            }
        }
    }
}
