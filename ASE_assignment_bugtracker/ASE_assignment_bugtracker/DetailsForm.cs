﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ASE_assignment_bugtracker.Classes;

namespace ASE_assignment_bugtracker
{
    /* This form serves two purposes: to show the details of a selected Bug from the MainForm in a better displayed way,
     * as well as showing the details of a selected BugAudit from the AuditHistoryForm in a better way, and allowing 
     * audits of a selected Bug to be performed. This is done by using the same controls, but when the details are shown
     * then the audit-controls are set to read-only, when an audit is to be performed these controls can be filled in 
     * again. Additionally, when only the details of the Bug itself must be shown, the entire-audit part is hidden. */
    public partial class DetailsForm : Form
    {
        private Bug bug;
        private DBConnector db;
        public DetailsForm(Bug selectedBug, bool audit = false, BugAudit selectedAudit = null)
        {
            InitializeComponent();
            if (audit || selectedAudit != null) grpAudit.Visible = true; /* if the form is in audit-mode, show the audit-
                                                                          * related controls */
            bug = selectedBug;
            displayBugDetails();
            if (selectedAudit != null) displayAuditDetails(selectedAudit);
            db = new DBConnector();
        }

        private void displayBugDetails()
        {
            /* this method displays the details of the selected Bug in their respective controls on the form */
            txtID.Text = bug.ID.ToString();
            txtStatus.Text = bug.Status;
            txtTitle.Text = bug.BugDetails.Title;
            txtDescription.Text = bug.BugDetails.Description;
            txtSourceFile.Text = bug.BugDetails.SourceFile;
            txtMethodName.Text = bug.BugDetails.MethodName;
            txtLineStart.Text = (bug.BugDetails.LineNumberStart == -1) ? string.Empty : bug.BugDetails.LineNumberStart.ToString();
            txtLineEnd.Text = (bug.BugDetails.LineNumberEnd == -1) ? string.Empty : bug.BugDetails.LineNumberEnd.ToString();
            txtProjectName.Text = bug.BugDetails.ProjectName;
            txtReportedBy.Text = bug.BugDetails.ReportedBy;
            txtReportDate.Text = bug.BugDetails.ReportedDate.ToString();
        }

        private void displayAuditDetails(BugAudit audit)
        {
            /* this method displays the details of the selected BugAudit in their respective controls on the form,
             * after first displaying the Bug details of the related bug */
            displayBugDetails();
            chkResolved.Checked = audit.IsResolved;
            chkResolved.Enabled = false;
            chkResolved.Text = "Bug resolved";
            txtAuditDate.Text = audit.AuditDate.ToString();
            txtAuditDate.Visible = true;
            btnSubmit.Visible = false;
            txtAuditComment.Text = audit.Comment;
            txtAuditComment.ReadOnly = true;
            txtAuditComment.ForeColor = Color.Black;
            txtAuditComment.BackColor = Color.WhiteSmoke;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            /* submits a new BugAudit to the database. The option to submit an empty comment-field is deliberate,
             * in the situation where an auditor quickly wanted to update the status of a bug, or because no comment 
             * is necessary*/
            string auditorName = Microsoft.VisualBasic.Interaction.InputBox("Please supply your name below:", "Bug reporter name", "UserName");
            if (String.IsNullOrWhiteSpace(auditorName))
            {
                MessageBox.Show("Please supply a valid name.");
                return;
            }

            BugAudit newAudit = new BugAudit(bug, DateTime.Now, auditorName, chkResolved.Checked, txtAuditComment.Text.Trim());

            db.insertNewAudit(newAudit);
            MessageBox.Show("The audit has been performed and saved.");
        }
    }
}
