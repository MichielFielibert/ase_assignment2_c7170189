﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ASE_assignment_bugtracker.Classes
{
    /*
        This class serves to connect with the database containing the Bug and Audit information, offering the ability
        to store new data (new reported Bugs and new performed audits), retrieve the existing Bugs and Audits from their
        respective tables, and update existing records. All this functionality has been condensed in this class, as
        the other classes don't need to see the internal structure of how these operations are performed.
     */
    class DBConnector
    {
        SqlConnection mySqlConnection;

        public DBConnector()
        {
            // this SqlConnection will be used in all of the methods below to connect to the database on my disk.
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Temp\Bugtracker.mdf;Integrated Security=True;Connect Timeout=30");
        }

        public void insertNewBug(Bug bug)
        {
            /* this method creates a new record in the database for the Bug object that is passed as parameter.
               the method is called when a new bug is reported and submitted.*/
            SqlCommand cmd = new SqlCommand("INSERT INTO Bug (status, title, description, sourcefile, methodName, " + 
                "startLineNumber, endLineNumber, projectName, reporterName, reportDate)" + 
                " VALUES (@status, @title, @description, @sourcefile, @methodName, @startLineNumber, @endLineNumber, " + 
                "@projectName, @reporterName, @reportDate)");
            cmd.CommandType = CommandType.Text;
            cmd.Connection = mySqlConnection;
            cmd.Parameters.AddWithValue("@status", bug.Status);
            cmd.Parameters.AddWithValue("@title", bug.BugDetails.Title);
            cmd.Parameters.AddWithValue("@description", bug.BugDetails.Description);
            cmd.Parameters.AddWithValue("@sourcefile", bug.BugDetails.SourceFile);
            cmd.Parameters.AddWithValue("@methodName", bug.BugDetails.MethodName);
            cmd.Parameters.AddWithValue("@startLineNumber", bug.BugDetails.LineNumberStart);
            cmd.Parameters.AddWithValue("@endLineNumber", bug.BugDetails.LineNumberEnd);
            cmd.Parameters.AddWithValue("@projectName", bug.BugDetails.ProjectName);
            cmd.Parameters.AddWithValue("@reporterName", bug.BugDetails.ReportedBy);
            cmd.Parameters.AddWithValue("@reportDate", bug.BugDetails.ReportedDate);
            mySqlConnection.Open(); // SqlConnection is opened
            cmd.ExecuteNonQuery();  /* query that has been established earlier and filled with values is executed, 
                                     * effectively inserting the new Bug into the database*/
            mySqlConnection.Close(); /* SqlConnection is closed when the operation for which it is needed has been
                                      * performed. Important to close this connection immediately to avoid issues where
                                      * other methods require the connection */
        }

        public Bug getBug(int id)
        {
            /* This method retrieves a single Bug object from the database with a SQL Select query, based on the id of
             * the bug that has been passed as parameter. */
            String selcmd = "SELECT * FROM Bug where id = @id";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlConnection.Open();
            mySqlCommand.Parameters.AddWithValue("@id", id);
            SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            Bug b = null;
            while (mySqlDataReader.Read())
            {
                string title = (string)mySqlDataReader["title"];
                string description = (string)mySqlDataReader["description"];
                /* the ternary operators below serve as an inline if-statement, in the case that the bug-record does not
                 * deliver usable values. They check if the values are null (because null values are allowed for these
                 * fields), and replace them with other values that the rest of the application can use to know that
                 * no value has been submitted for these fields in the database. */
                string sourceFile = (mySqlDataReader["sourcefile"] == null) ? string.Empty : mySqlDataReader["sourcefile"].ToString();
                string methodName = (mySqlDataReader["methodName"] == null) ? string.Empty : mySqlDataReader["methodName"].ToString();
                int lineNumberStart = (mySqlDataReader["startLineNumber"] == DBNull.Value) ? -1 : (int)mySqlDataReader["startLineNumber"];
                int lineNumberEnd = (mySqlDataReader["endLineNumber"] == DBNull.Value) ? -1 : (int)mySqlDataReader["endLineNumber"];
                string projectName = (mySqlDataReader["projectName"] == null) ? string.Empty : mySqlDataReader["projectName"].ToString();
                string reporterName = (string)mySqlDataReader["reporterName"];
                DateTime reportDate = (DateTime)mySqlDataReader["reportDate"];

                BugDetails bd = new BugDetails(title,
                    description,
                    sourceFile,
                    methodName,
                    lineNumberStart,
                    lineNumberEnd,
                    projectName,
                    reporterName,
                    reportDate);

                b = new Bug((int)mySqlDataReader["id"],
                    (string)mySqlDataReader["status"],
                    bd); /* after the BugDetails object has been constructed, the Bug object is constructed containing
                          * these details, after which this object is returned. */

                
            }
            mySqlConnection.Close();
            return b;
        }

        public List<Bug> getAllBugs()
        {
            /* this method retrieves all of the Bugs in the database, works in the same way as the method above but 
             * instead of retrieving a single bug based on the id that was given, it just executes a general select-
             * query and returns the results*/
            List<Bug> resultList = new List<Bug>();
            String selcmd = "SELECT * FROM Bug";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlConnection.Open();
            SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            while (mySqlDataReader.Read())
            {
                string title = (string)mySqlDataReader["title"];
                string description = (string)mySqlDataReader["description"];
                string sourceFile = (mySqlDataReader["sourcefile"] == null) ? string.Empty : mySqlDataReader["sourcefile"].ToString();
                string methodName = (mySqlDataReader["methodName"] == null) ? string.Empty : mySqlDataReader["methodName"].ToString();
                int lineNumberStart = (mySqlDataReader["startLineNumber"] == DBNull.Value) ? -1 : (int)mySqlDataReader["startLineNumber"];
                int lineNumberEnd = (mySqlDataReader["endLineNumber"] == DBNull.Value) ? -1 : (int)mySqlDataReader["endLineNumber"];
                string projectName = (mySqlDataReader["projectName"] == null) ? string.Empty : mySqlDataReader["projectName"].ToString();
                string reporterName = (string)mySqlDataReader["reporterName"];
                DateTime reportDate = (DateTime)mySqlDataReader["reportDate"];

                BugDetails bd = new BugDetails(title,
                    description,
                    sourceFile,
                    methodName,
                    lineNumberStart,
                    lineNumberEnd,
                    projectName,
                    reporterName,
                    reportDate);

                Bug b = new Bug((int)mySqlDataReader["id"], 
                    (string)mySqlDataReader["status"], 
                    bd);

                resultList.Add(b);
            }
            mySqlConnection.Close();
            return resultList;
        }

        public void insertNewAudit(BugAudit audit)
        {
            /* This method creates a new Audit record for a newly performed audit of a bug, based on the information 
             * contained in the BugAudit-object passed as parameter.*/
            SqlCommand cmd = new SqlCommand("INSERT INTO Audit (bug, auditorName, resolved, comment, auditDate)" +
                " VALUES (@bug, @auditorName, @resolved, @comment, @auditDate)");
            cmd.CommandType = CommandType.Text;
            cmd.Connection = mySqlConnection;
            cmd.Parameters.AddWithValue("@bug", audit.Bug.ID);
            cmd.Parameters.AddWithValue("@auditorName", audit.AuditedBy);
            //int boolToDb = (audit.IsResolved) ? 1 : 0;
            cmd.Parameters.AddWithValue("@resolved", (audit.IsResolved) ? 1 : 0);
            cmd.Parameters.AddWithValue("@comment", audit.Comment);
            cmd.Parameters.AddWithValue("@auditDate", audit.AuditDate);
            mySqlConnection.Open();
            cmd.ExecuteNonQuery();
            mySqlConnection.Close();
            updateBugStatus(audit.Bug, audit.IsResolved); // see below
        }

        private void updateBugStatus(Bug bug, bool resolved)
        {
            /* this method is called at the end of the insertNewAudit-method, to update the status of the bug for which
             * the audit has been performed. If the audit resolved the bug, the status will be set to closed, otherwise
             * set to open.*/
            SqlCommand cmd = new SqlCommand("UPDATE Bug Set status = @newStatus Where id = @bugID");
            cmd.CommandType = CommandType.Text;
            cmd.Connection = mySqlConnection;
            cmd.Parameters.AddWithValue("@newStatus", (resolved) ? "Closed" : "Open");
            cmd.Parameters.AddWithValue("@bugID", bug.ID);
            mySqlConnection.Open();
            cmd.ExecuteNonQuery();
            mySqlConnection.Close();
        }

        public List<BugAudit> getBugAudits(Bug bug)
        {
            /* similar to the getAllBugs()-method, this function returns a list of all the BugAudits in the database. */
            List<BugAudit> resultList = new List<BugAudit>();
            String selcmd = "SELECT * FROM Audit where bug = @bugID";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlConnection.Open();
            mySqlCommand.Parameters.AddWithValue("@bugID", bug.ID);
            SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            while (mySqlDataReader.Read())
            {
                int id = (int)mySqlDataReader["id"];
                DateTime auditDate = (DateTime)mySqlDataReader["auditDate"];
                string auditorName = (string)mySqlDataReader["auditorName"];
                bool resolved = (bool)mySqlDataReader["resolved"];
                string comment = (mySqlDataReader["comment"] == null) ? string.Empty : mySqlDataReader["comment"].ToString();

                BugAudit ba = new BugAudit(id, bug, auditDate, auditorName, resolved, comment);
                resultList.Add(ba);
            }
            mySqlConnection.Close();
            return resultList;
        }

        public BugAudit getBugAudit(int id)
        {
            /* similar to the getBug-method, this method retrieves a single BugAudit from the database, based on the given id. */
            String selcmd = "SELECT * FROM Audit where id = @auditID";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlConnection.Open();
            mySqlCommand.Parameters.AddWithValue("@auditID", id);
            SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            BugAudit ba = null;
            if (mySqlDataReader.Read())
            {
                DateTime auditDate = (DateTime)mySqlDataReader["auditDate"];
                string auditorName = (string)mySqlDataReader["auditorName"];
                bool resolved = (bool)mySqlDataReader["resolved"];
                string comment = (mySqlDataReader["comment"] == null) ? string.Empty : mySqlDataReader["comment"].ToString();
                int bugID = (int)mySqlDataReader["bug"];
                mySqlConnection.Close();
                /* important to note here is that we already closed the connection here instead of at the end of the 
                 * method, because the next line calls getBug(), which also requires the SqlConnection. Since only one
                 * record is expected, it is safe to close the connection at this point already, as there won't be any
                 * looping through multiple records - only one Read needs to be performed. */
                Bug auditBug = getBug(bugID);
                ba = new BugAudit(id, auditBug, auditDate, auditorName, resolved, comment);
            }
            //mySqlConnection.Close();
            return ba;
        }
    }
}
