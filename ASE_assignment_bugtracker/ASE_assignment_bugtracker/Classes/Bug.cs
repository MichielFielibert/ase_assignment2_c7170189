﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_assignment_bugtracker.Classes
{
    /* 
        This class holds the id (when retrieved from database) and status of the bug-objects, as well as the
        BugDetails object for each bug (a sub-object of Bug containing additional bug details)
     */
    public class Bug
    {
        private int id;
        private string status;
        private BugDetails bugDetails;

        internal BugDetails BugDetails
        {
            get { return bugDetails; }
        }

        public int ID
        {
            get { return id; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public Bug(string status, BugDetails bd) 
        {
            this.status = status;
            this.bugDetails = bd;
        }

        public Bug(int id, string status, BugDetails bd)
        {
            this.id = id;
            this.status = status;
            this.bugDetails = bd;
        }

        public override string ToString()
        {
            return this.id + " " + this.status + " " + this.bugDetails.Title + " " + this.bugDetails.Description;
        }

        public bool isOpen()
        {
            if (this.Status == "Closed")
            {
                return false;
            }
            else
            {
                return true;
            }
        }


    }


}
