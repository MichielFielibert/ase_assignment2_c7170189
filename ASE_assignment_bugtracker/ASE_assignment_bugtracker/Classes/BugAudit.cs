﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_assignment_bugtracker.Classes
{
    /*
        This class contains the information (and id, when retrieved from database) of each bug audit that was performed.       
     */
    public class BugAudit
    {
        private int id;
        private Bug bug;       
        private DateTime auditDate;
        private string auditedBy;
        private bool isResolved = false; // yes or no, if yes then bug.Status = fixed as well (else remains in progress)
        private string comment;

        public int ID
        {
            get { return id; }
        }

        internal Bug Bug
        {
            get { return bug; }
        }
        public DateTime AuditDate
        {
            get { return auditDate; }
        }
        public string AuditedBy
        {
            get { return auditedBy; }
        }
        public bool IsResolved
        {
            get { return isResolved; }
            set { isResolved = value; }
        }
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        public BugAudit(Bug bug, DateTime date, string auditedby, bool resolved, string comment)
        {
            this.bug = bug;
            this.auditDate = date;
            this.auditedBy = auditedby;
            this.isResolved = resolved;
            this.comment = comment;
        }

        public BugAudit(int id, Bug bug, DateTime date, string auditedby, bool resolved, string comment)
        {
            this.id = id;
            this.bug = bug;
            this.auditDate = date;
            this.auditedBy = auditedby;
            this.isResolved = resolved;
            this.comment = comment;
        }
    }
}
