﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_assignment_bugtracker.Classes
{
    /*
        This class contains more details about the reported bugs, with half of its fields not required to be filled in.
        This class exists to separate the user-input of each reported bug from the parent Bug-objects.
     */
    public class BugDetails
    {
        private string title;
        private string description;
        private string sourceFile;
        private string methodName;
        private int lineNumberStart;
        private int lineNumberEnd;
        private string projectName;
        private string reportedBy;
        private DateTime reportedDate;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string SourceFile
        {
            get { return sourceFile; }
        }
        public string MethodName
        {
            get { return methodName; }
        }
        public int LineNumberStart
        {
            get { return lineNumberStart; }
        }
        public int LineNumberEnd
        {
            get { return lineNumberEnd; }
        }
        public string ProjectName
        {
            get { return projectName; }
        }
        public string ReportedBy
        {
            get { return reportedBy; }
        }
        public DateTime ReportedDate
        {
            get { return reportedDate; }
        }

        public BugDetails() { }

        public BugDetails(string title, string desc, string sourcefile, string methodname, int lstart, int lend, string projectname, string reportedby, DateTime date)
        {
            this.title = title;
            this.description = desc;
            this.sourceFile = sourcefile;
            this.methodName = methodname;
            this.lineNumberStart = lstart;
            this.lineNumberEnd = lend;
            this.projectName = projectname;
            this.reportedBy = reportedby;
            this.reportedDate = date;
        }



    }
}
