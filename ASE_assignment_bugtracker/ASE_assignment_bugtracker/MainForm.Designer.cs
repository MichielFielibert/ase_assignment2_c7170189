﻿namespace ASE_assignment_bugtracker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabReportBug = new System.Windows.Forms.TabPage();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.txtLineNumberEnd = new System.Windows.Forms.TextBox();
            this.txtLineNumberStart = new System.Windows.Forms.TextBox();
            this.txtMethodName = new System.Windows.Forms.TextBox();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnViewBugDetails = new System.Windows.Forms.Button();
            this.btnViewBugHistory = new System.Windows.Forms.Button();
            this.btnStartBugAudit = new System.Windows.Forms.Button();
            this.lstBugs = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1.SuspendLayout();
            this.tabReportBug.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabReportBug);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(881, 440);
            this.tabControl1.TabIndex = 0;
            // 
            // tabReportBug
            // 
            this.tabReportBug.BackColor = System.Drawing.SystemColors.Control;
            this.tabReportBug.Controls.Add(this.btnClear);
            this.tabReportBug.Controls.Add(this.btnSubmit);
            this.tabReportBug.Controls.Add(this.groupBox1);
            this.tabReportBug.Controls.Add(this.txtDescription);
            this.tabReportBug.Controls.Add(this.txtTitle);
            this.tabReportBug.Controls.Add(this.label2);
            this.tabReportBug.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabReportBug.Location = new System.Drawing.Point(4, 29);
            this.tabReportBug.Name = "tabReportBug";
            this.tabReportBug.Padding = new System.Windows.Forms.Padding(3);
            this.tabReportBug.Size = new System.Drawing.Size(873, 407);
            this.tabReportBug.TabIndex = 0;
            this.tabReportBug.Text = "Report bug";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(612, 355);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(112, 36);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(740, 355);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(112, 36);
            this.btnSubmit.TabIndex = 11;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtProjectName);
            this.groupBox1.Controls.Add(this.txtLineNumberEnd);
            this.groupBox1.Controls.Add(this.txtLineNumberStart);
            this.groupBox1.Controls.Add(this.txtMethodName);
            this.groupBox1.Controls.Add(this.txtSourceFile);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(475, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(378, 323);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Additional details (if known/applicable)";
            // 
            // txtProjectName
            // 
            this.txtProjectName.Location = new System.Drawing.Point(10, 272);
            this.txtProjectName.MaxLength = 75;
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(362, 27);
            this.txtProjectName.TabIndex = 16;
            // 
            // txtLineNumberEnd
            // 
            this.txtLineNumberEnd.Location = new System.Drawing.Point(250, 210);
            this.txtLineNumberEnd.Name = "txtLineNumberEnd";
            this.txtLineNumberEnd.Size = new System.Drawing.Size(122, 27);
            this.txtLineNumberEnd.TabIndex = 15;
            // 
            // txtLineNumberStart
            // 
            this.txtLineNumberStart.Location = new System.Drawing.Point(250, 174);
            this.txtLineNumberStart.Name = "txtLineNumberStart";
            this.txtLineNumberStart.Size = new System.Drawing.Size(122, 27);
            this.txtLineNumberStart.TabIndex = 14;
            // 
            // txtMethodName
            // 
            this.txtMethodName.Location = new System.Drawing.Point(10, 134);
            this.txtMethodName.MaxLength = 75;
            this.txtMethodName.Name = "txtMethodName";
            this.txtMethodName.Size = new System.Drawing.Size(362, 27);
            this.txtMethodName.TabIndex = 13;
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.Location = new System.Drawing.Point(10, 66);
            this.txtSourceFile.MaxLength = 75;
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.Size = new System.Drawing.Size(362, 27);
            this.txtSourceFile.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(221, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Code block end line number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Project name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 177);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Code block start line number:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Source file:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Method name:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(17, 81);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(436, 257);
            this.txtDescription.TabIndex = 9;
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(17, 15);
            this.txtTitle.MaxLength = 100;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(436, 27);
            this.txtTitle.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description:";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.btnViewBugDetails);
            this.tabPage2.Controls.Add(this.btnViewBugHistory);
            this.tabPage2.Controls.Add(this.btnStartBugAudit);
            this.tabPage2.Controls.Add(this.lstBugs);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(873, 407);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "View bugs";
            // 
            // btnViewBugDetails
            // 
            this.btnViewBugDetails.Location = new System.Drawing.Point(180, 347);
            this.btnViewBugDetails.Name = "btnViewBugDetails";
            this.btnViewBugDetails.Size = new System.Drawing.Size(148, 54);
            this.btnViewBugDetails.TabIndex = 3;
            this.btnViewBugDetails.Text = "View bug details";
            this.btnViewBugDetails.UseVisualStyleBackColor = true;
            this.btnViewBugDetails.Click += new System.EventHandler(this.btnViewBugDetails_Click);
            // 
            // btnViewBugHistory
            // 
            this.btnViewBugHistory.Location = new System.Drawing.Point(345, 347);
            this.btnViewBugHistory.Name = "btnViewBugHistory";
            this.btnViewBugHistory.Size = new System.Drawing.Size(148, 54);
            this.btnViewBugHistory.TabIndex = 2;
            this.btnViewBugHistory.Text = "View bug history";
            this.btnViewBugHistory.UseVisualStyleBackColor = true;
            this.btnViewBugHistory.Click += new System.EventHandler(this.btnViewBugHistory_Click);
            // 
            // btnStartBugAudit
            // 
            this.btnStartBugAudit.Location = new System.Drawing.Point(8, 347);
            this.btnStartBugAudit.Name = "btnStartBugAudit";
            this.btnStartBugAudit.Size = new System.Drawing.Size(148, 54);
            this.btnStartBugAudit.TabIndex = 1;
            this.btnStartBugAudit.Text = "Start bug audit";
            this.btnStartBugAudit.UseVisualStyleBackColor = true;
            this.btnStartBugAudit.Click += new System.EventHandler(this.btnStartBugAudit_Click);
            // 
            // lstBugs
            // 
            this.lstBugs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader9,
            this.columnHeader5});
            this.lstBugs.FullRowSelect = true;
            this.lstBugs.Location = new System.Drawing.Point(8, 16);
            this.lstBugs.MultiSelect = false;
            this.lstBugs.Name = "lstBugs";
            this.lstBugs.Size = new System.Drawing.Size(859, 325);
            this.lstBugs.TabIndex = 0;
            this.lstBugs.UseCompatibleStateImageBehavior = false;
            this.lstBugs.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Status";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Title";
            this.columnHeader3.Width = 120;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Description";
            this.columnHeader4.Width = 200;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Reported by";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Reported on";
            this.columnHeader5.Width = 173;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 445);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "BugTracker";
            this.tabControl1.ResumeLayout(false);
            this.tabReportBug.ResumeLayout(false);
            this.tabReportBug.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabReportBug;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMethodName;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.TextBox txtLineNumberStart;
        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.TextBox txtLineNumberEnd;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.ListView lstBugs;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button btnStartBugAudit;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnViewBugHistory;
        private System.Windows.Forms.Button btnViewBugDetails;
    }
}

